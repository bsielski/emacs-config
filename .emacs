(add-to-list 'load-path "~/.emacs.d/my_config/")
(load-library "melpa")
(package-initialize)

(load-library "keys")
(load-library "enable-resizing-neo-tree-window")
(load-library "no-tabs")
(load-library "visual-line-mode-hooks")
(load-library "flyspell-mode-hooks")
(load-library "about-backup-files")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(custom-enabled-themes (quote (wombat)))
 '(inhibit-startup-screen t)
 '(package-selected-packages (quote (go-mode neotree))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
